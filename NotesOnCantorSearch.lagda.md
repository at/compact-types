```agda
{-# OPTIONS --without-K --exact-split #-}

module NotesOnCantorSearch where

open import Level hiding (suc)
open import NotesOnTypeCompactness
open import Data.Empty
open import Data.Product
open import Data.Nat
open import Data.Unit
open import Relation.Binary.PropositionalEquality
open import Function
open ≡-Reasoning
open import Two

private
  variable
    ℓ : Level

ε𝟚 : (𝟚 → 𝟚) → 𝟚
ε𝟚 p = p 𝟎

A𝟚 : (𝟚 → 𝟚) → 𝟚
A𝟚 p = p (ε𝟚 p)

𝟚-compact∙ : compact∙ 𝟚
𝟚-compact∙ p = ε𝟚 p , γ
 where
  γ : A𝟚 p ≡ 𝟏 → (x : 𝟚) → p x ≡ 𝟏
  γ eq 𝟎 with p 𝟎 in eq₁
  γ eq 𝟎 | 𝟎 = 𝟎 ≡⟨ sym eq₁ ⟩ p 𝟎 ≡⟨ eq ⟩ 𝟏 ∎
  γ eq 𝟎 | 𝟏 = refl
  γ eq 𝟏 with p 𝟎 in eq₁
  γ eq 𝟏 | 𝟎 = ⊥-elim (𝟎≠x=𝟏 (p 𝟎) eq₁ eq)
  γ eq 𝟏 | 𝟏 = eq
```

```agda
Cantor = ℕ → 𝟚
```

```agda
head : Cantor → 𝟚
head α = α 0

tail : Cantor → Cantor
tail α = λ n → α (suc n)

_∷_ : 𝟚 → Cantor → Cantor
(b ∷ α) zero    = b
(b ∷ α) (suc i) = α i

_=[_]=_ : Cantor → ℕ → Cantor → Set₀
α =[ zero  ]= β = ⊤
α =[ suc n ]= β = head α ≡ head β × tail α =[ n ]= tail β
```

```agda
_is-a-mod-of-uc-of_ : ℕ → (Cantor → 𝟚) → Set₀
n is-a-mod-of-uc-of p = (α β : Cantor) → α =[ n ]= β → p α ≡ p β

uniformly-continuous : (Cantor → 𝟚) → Set₀
uniformly-continuous p = Σ[ n ∈ ℕ ] n is-a-mod-of-uc-of p
```

```agda
is-constant : (Cantor → 𝟚) → Set₀
is-constant p = (α β : Cantor) → p α ≡ p β
```

```agda
mod-of-uc-zero-implies-constancy : (p : Cantor → 𝟚)
                                 → 0 is-a-mod-of-uc-of p
                                 → is-constant p
mod-of-uc-zero-implies-constancy p ζ α β = ζ α β tt

constancy-implies-mod-of-uc-zero : (p : Cantor → 𝟚)
                                 → is-constant p
                                 → 0 is-a-mod-of-uc-of p
constancy-implies-mod-of-uc-zero p φ α β _ = φ α β
```

```agda
lemma : (p : Cantor → 𝟚) (n : ℕ) (b : 𝟚) (α β : Cantor)
      → (b ∷ α) =[ suc n ]= (b ∷ β)
      → α =[ n ]= β
lemma p zero    b α β eq = tt
lemma p (suc ℕ.zero) b α β eq = proj₁ (proj₂ eq) , tt
lemma p (suc (suc n)) b α β eq = (proj₁ (proj₂ eq)) , bar
  where
    bar : tail α =[ suc n ]= tail β
    bar = proj₂ (proj₂ eq)

cons-decreases-mod-of-uc : (p : Cantor → 𝟚)
                         → (n : ℕ)
                         → (suc n) is-a-mod-of-uc-of p
                         → (b : 𝟚) → n is-a-mod-of-uc-of (p ∘ b ∷_)
cons-decreases-mod-of-uc p n φ b α β eq = φ (b ∷ α) (b ∷ β) (refl , eq)
```

```agda
module Search (c₀ : Cantor) where

  ε-cantor : ℕ → (Cantor → 𝟚) → Cantor
  A-cantor : ℕ → (Cantor → 𝟚) → 𝟚

  ε-cantor zero    p = c₀
  ε-cantor (suc n) p = b₀ ∷ ε-cantor n (λ α → p (b₀ ∷ α))
    where
      b₀ = ε𝟚 λ b → A-cantor n (λ α → p (b ∷ α))

  A-cantor n p = p (ε-cantor n p)

  A-condition₁ : Set₀
  A-condition₁ =
    (p : Cantor → 𝟚) →
      ((n , _) : uniformly-continuous p) →
        A-cantor n p ≡ 𝟏 →
          (α : Cantor) → p α ≡ 𝟏

  A-condition₂ : Set₀
  A-condition₂ =
    (p : Cantor → 𝟚) →
      ((n , _) : uniformly-continuous p) →
        ((α : Cantor) → p α ≡ 𝟏) →
          A-cantor n p ≡ 𝟏

  A-satisfies-spec₁ : A-condition₁
  A-satisfies-spec₁ p = uncurry γ
    where
      γ : (n : ℕ)
        → n is-a-mod-of-uc-of p
        → A-cantor n p ≡ 𝟏 → (α : Cantor) → p α ≡ 𝟏
      γ zero    ζ eq α = p α    ≡⟨ mod-of-uc-zero-implies-constancy p ζ α c₀ ⟩
                         p c₀   ≡⟨ eq                                        ⟩
                         𝟏      ∎
      γ (suc n) ζ eq α = p α                     ≡⟨ cong p δ ⟩
                         p (ε-cantor (suc n) p)  ≡⟨ refl     ⟩
                         A-cantor (suc n) p      ≡⟨ eq       ⟩
                         𝟏                       ∎
        where
          δ : α ≡ ε-cantor (suc n) p
          δ = {!!}

  A-satisfies-spec₂ : A-condition₂
  A-satisfies-spec₂ p (n , ζ) φ = φ (ε-cantor n p)
```

```agda
  Cantor-uniformly-searchable : (p : Cantor → 𝟚)
                              → uniformly-continuous p
                              → Σ[ α₀ ∈ Cantor ] (p α₀ ≡ 𝟏 → (α : Cantor) → p α ≡ 𝟏)
  Cantor-uniformly-searchable p υ@(n , ζ) = ε-cantor n p , A-satisfies-spec₁ p υ
```
