```agda
{-# OPTIONS --safe --without-K #-}

module NotesOnTypeCompactness where

open import Level
open import Two
open import Data.Sum
open import Data.Product
open import Relation.Binary.PropositionalEquality
open import Data.Empty

private
  variable
    ℓ : Level
```

A type is called compact if it is *exhaustively searchable*. However, many
closely related, but different, notions of compactness are explored in modules
[CompactTypes][1] and [WeaklyCompactTypes][2].

There are at least three definitions of compactness.

1. `Σ-compactness`
2. `Π-compactness`
3. `∃-compactness`

## Σ-compactness

```agda
Σ-compact : Set ℓ → Set ℓ
Σ-compact X =
  (p : X → 𝟚) →
    Σ[ x ∈ X ] p x ≡ 𝟎 ⊎ ((x : X) → p x ≡ 𝟏)
```

## Pointed compactness

```agda
compact∙ : Set ℓ → Set ℓ
compact∙ X = (p : X → 𝟚) → Σ[ x₀ ∈ X ] (p x₀ ≡ 𝟏 → (x : X) → p x ≡ 𝟏)
```

Notice that this is a lot like the Drinker Paradox. Some type `X` ("pub")
satisfies the notion of `compact∙` iff for every predicate `p : X → 𝟚`, there is
some `x₀` ("man") such that if `x₀` drinks then everyone in the pub drinks.

```agda
compact-and-pointed-gives-compact∙ : {X : Set ℓ} → Σ-compact X → X → compact∙ X
compact-and-pointed-gives-compact∙ κ x₀ p with κ p
compact-and-pointed-gives-compact∙ κ x₀ p | inj₁ (x₁ , φ)  with p x₁ in eq
compact-and-pointed-gives-compact∙ κ x₀ p | inj₁ (x₁ , φ)  | 𝟎 = x₁ , λ r → ⊥-elim (𝟎≠x=𝟏 (p x₁) eq r)
compact-and-pointed-gives-compact∙ κ x₀ p | inj₁ (x₁ , ()) | 𝟏
compact-and-pointed-gives-compact∙ κ x₀ p | inj₂ φ             = x₀ , (λ _ → φ)
```

```agda
compact∙-gives-compact : {X : Set ℓ} → compact∙ X → Σ-compact X
compact∙-gives-compact {X = X} ϑ p = γ
  where
    x₀ = proj₁ (ϑ p)

    η : p x₀ ≡ 𝟏 → (x : X) → p x ≡ 𝟏
    η = proj₂ (ϑ p)

    γ : (Σ[ x ∈ X ] p x ≡ 𝟎) ⊎ ((x : X) → p x ≡ 𝟏)
    γ with p x₀ in eq
    γ | 𝟎 = inj₁ (x₀ , eq)
    γ | 𝟏 = inj₂ (η eq)
```


```agda
record Searchable (X : Set ℓ) : Set ℓ where
  field
    ε : (X → 𝟚) → X

    ε-is-a-selection-function : (p : X → 𝟚)
                              → p (ε p) ≡ 𝟏
                              → (x : X) → p x ≡ 𝟏
```

```agda
record Exhaustible (X : Set ℓ) : Set ℓ where
  field
    𝔸 : (X → 𝟚) → 𝟚

    𝔸-spec₁ : (p : X → 𝟚)
            → 𝔸 p ≡ 𝟏 → ((x : X) → p x ≡ 𝟏)
    𝔸-spec₂ : (p : X → 𝟚)
            → ((x : X) → p x ≡ 𝟏) → 𝔸 p ≡ 𝟏
```
