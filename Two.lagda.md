```agda
{-# OPTIONS --safe --without-K #-}

module Two where

open import Data.Empty
open import Relation.Binary.PropositionalEquality

data 𝟚 : Set where
  𝟎 𝟏 : 𝟚

𝟎≠x=𝟏 : (x : 𝟚) → x ≡ 𝟎 → x ≡ 𝟏 → ⊥
𝟎≠x=𝟏 𝟎 refl ()
```
