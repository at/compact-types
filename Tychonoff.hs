module Tychonoff where

type Searcher   a = (a -> Bool) -> a
type Quantifier a = (a -> Bool) -> Bool

type Nat = Integer

forsome, forevery :: Searcher a -> Quantifier a
forsome  k p = p (k p)
forevery k p = not (forsome k (\x -> not (p x)))

times :: Searcher a -> Searcher b -> Searcher (a, b)
times k k' p = (x, x')
   where
     x  = k (\x -> forsome k' (\x' -> p (x, x')))
     x' = k' (\x' -> p (x, x'))

prod :: (Nat -> Searcher a) -> Searcher (Nat -> a)
prod e p n = e n (\x -> r n x (prod (\i -> e (n+i+1)) (r n x)))
  where
    r n x a = p (\i -> if i < n then
                         prod e p i
                       else if i == n then
                         x
                       else
                         a (i-n-1))

finprod :: [Searcher a] -> Searcher [a]
finprod []     p = []
finprod (e:es) p = x0 : finprod es (p . (x0 :))
  where
    x0 = e(\x -> p(x : finprod es (p . (x :))))

e2 :: Searcher Bool
e2 p = p True

search :: Searcher (Nat -> Bool)
search = prod (\_ -> e2)

type Cantor = Nat -> Bool

p1 :: Cantor -> Bool
p1 p = p 21 == True

data Formula = Var Nat
             | Formula :/\: Formula
             | Formula :\/: Formula
             | Formula :=>: Formula
             | Not Formula

(|=) :: Cantor -> Formula -> Bool
v |= (Var n)    = v n
v |= (p :/\: q) = v |= p && v |= q
v |= (p :\/: q) = v |= p || v |= q
v |= (p :=>: q) = if v |= p then v |= q else True
v |= (Not p)    = not (v |= p)

eval :: Formula -> Cantor -> Bool
eval p v =  (|=) v p

phi :: Formula
phi = (Var 0) :\/: (Not (Var 0))

(#) :: Bool -> Cantor -> Cantor
(#) b alpha 0 = b
(#) b alpha n = alpha (n - 1)

cantorTree :: Nat -> [Cantor]
cantorTree 0 = [\_ -> False]
cantorTree n = concat [ [True # alpha , False # alpha] | alpha <- cantorTree (n-1) ]

agree :: Integer -> (Nat -> Bool) -> (Nat -> Bool) -> Bool
agree n a b = all id [ a i == b i | i <- [0..n] ]

(-->) :: Bool -> Bool -> Bool
p --> q = not p || q

eq :: Nat -> Cantor -> Cantor -> Bool
eq 0 a b = True
eq n a b = a (n - 1) == b (n - 1) &&  eq (n - 1) a b

least :: (Nat -> Bool) -> Nat
least p = if p 0 then 0 else 1+least(\n -> p(n+1))

modulus :: (Cantor -> Bool) -> Nat
modulus f = least (\n -> forevery search (\a -> forevery search (\b -> (eq n a b) --> (f a == f b))))

allModels :: Formula -> [Cantor]
allModels phi = let n = modulus (eval phi) in [ v | v <- cantorTree (n+1), v |= phi ]

isSatisfiable :: Formula -> Bool
isSatisfiable p = search (\v -> v |= p) |= p

type NatInf = Cantor

zero :: NatInf
zero = \_ -> False

suc :: NatInf -> NatInf
suc a = True # a

to :: Nat -> NatInf
to 0 = zero
to n = suc (to (n-1))

tailc :: Cantor -> Cantor
tailc a = \i -> a (i + 1)

from :: NatInf -> Nat
from a = if not (a 0) then 0 else 1 + from (tailc a)

theorem :: (Cantor -> Cantor) -> (Cantor -> Nat -> Nat) -> Nat -> Nat
theorem f cont m = cont a0 m
  where
    p :: Cantor -> Bool
    p a = let
            n = cont a m
          in
            forevery search (\b -> agree n a b --> agree m (f a) (f b))

    a0 :: Cantor
    a0 = search p
